       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. KANTIDA.

       DATA DIVISION. 
       WORKING-STORAGE SECTION.    
       01  WEIGHT   PIC 99 VALUE ZEROS.
       01  HEIGHT   PIC 999 VALUE ZEROS.
       01  HEIGTH-METER PIC 9V99 VALUE ZEROS.
       01  RESULT PIC 99V99 VALUE ZEROS.
       01  RESULT-MESSAGE PIC X(50) VALUE SPACE.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Please enter your weight(kg): " WITH NO ADVANCING 
           ACCEPT WEIGHT

           DISPLAY "Please enter your height(cm): " WITH NO ADVANCING 
           ACCEPT HEIGHT 
           
           COMPUTE HEIGTH-METER = HEIGHT / 100
           COMPUTE RESULT = WEIGHT / (HEIGTH-METER * HEIGTH-METER)

           EVALUATE RESULT 
              WHEN 1 THRU 18.49 MOVE "Underweight" TO RESULT-MESSAGE   
              WHEN 18.50 THRU 24.99 MOVE "Normal(Healthy)" TO 
              RESULT-MESSAGE   
              WHEN 30 THRU 34.99 MOVE "Obesity Level 1" TO 
              RESULT-MESSAGE   
              WHEN 35 THRU 39.99 MOVE "Obesity Level 2" 
              TO RESULT-MESSAGE   
              WHEN 40 THRU 99.99 MOVE "Obesity Level 3" TO 
              RESULT-MESSAGE   

           END-EVALUATE
           DISPLAY "Your BMI is : " RESULT
           DISPLAY "Result is " RESULT-MESSAGE  
           
           GOBACK 
       .

